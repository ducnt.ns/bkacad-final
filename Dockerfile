FROM node:latest

LABEL "Email"="ducnt.BK1.Devopsonline23-04.2510@student.bkacad.edu.vn"

LABEL "Author"="Nguyen Thien Duc"

WORKDIR /app

COPY . . 

RUN npm install

EXPOSE 8080

CMD ["npm", "start"]
